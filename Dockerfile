FROM node:18-alpine3.18

WORKDIR /usr/src/app

COPY package*.json ./
RUN npm install
COPY images ./images
COPY index.html .
COPY server.js .

ENV PRODUCTS_SERVICE="products"
ENV SHOPPING_CART_SERVICE="shopping-cart"

EXPOSE 8000
CMD ["npm", "start"]
